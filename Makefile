DESTDIR ?= ${DISCIMAGE}
TARGET_ARCH ?= arm

SRCDIR = ./targetfs/${TARGET_ARCH}
INSTALL_DIR = ${DESTDIR}/opt/img-powervr-sdk

all:

install: 
	mkdir -p ${INSTALL_DIR}
	cp -ar ${SRCDIR}/* ${INSTALL_DIR}
